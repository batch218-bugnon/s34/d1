// This code will help us access contents of express module/package
	// A "Module" is a software component or part of a program that contains one or more routines
// It also allows us to access methods and functions that we will use to easily create an app/server
// We store our express module to variable we could easily access its keywords, functions and methods.
const express = require ("express");

// This code creates an application using express / a.k.a express application
	// app is our server
const app = express();

// For our applications server to run, we need a port tp listen to
const port = 3000;

// For our application could read JSON data
app.use(express.json());

// This allows your app to read data from forms 
// By default, information received from the url can only be received as string or an array
// By applying the option of "extended:true" this allows us to receive information in other data types, such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));

// This route expects to receive a GET request at the URI/endpoint "/hello"
app.get("/hello", (request, response) => {

	// This is the reponse that we will expect to receive if the get method is successful with the right endpoint	
	response.send("GET method suceess. \n Hello from /hello endpoint!");
});


app.post("/hello", (request, response) => {
	
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});


let users = [];

app.post("/signup", (request, response) => {
	if(request.body.username !== "" && request.body.password !== "") {
		users.push(request.body);
		console.log(users);
		response.send(`user ${request.body.username} successfully registered`);
		// Sending another response will result an error, only the first response will display
		//response.send("welcome admin");
	}
	else {
		response.send("Please input BOTH username and password");
	}

});

// Syntax:
/*
app.httpMethod("/endpoint", (request, response) => {
	// code block
})
*/

// We use put method to update a document
app.put("/change-password",(request, response) => {

	// We create a variable where we will store our response 
	let message;
		// The purpose of our loop is to check if our per element in users array if it match our request.body.username

		// initialization //condition // change of value
	for(let i=0; i < users.length; i++){
		if(request.body.username == users[i].username){

			// update an element object's password based on the input on the body 
			users[i].password = request.body.password 

			// We reassign what message we would like to receive as response
			message = `User ${request.body.username}'s password has been updated`;
			break;
		}
		else {
			// If there is no match we would like to receive a message that user does not exist 
			message = "User does not exist."
		}
	}
	// We display our updated array
	console.log(users);

	// We display our response based if there is a match (successfully updated the pssword) or if there no match (user does not exist)
	response.send(message);
});


// 1
// Create a route that GET request at the URI/endpoint "/home"
// Response that will be received should be "Welcome to the homepage"
// Code bellow ...
app.get("/home", (request, response) => {
	response.send("Welcome to the homepage");
});


// 2 
// Create a route that expects GET request at the URI/endpoint "/users"
// Response that will be received is a collection of users 

app.get("/users", (request, response) => {	
	response.send(users);
});
// 3
// Creae a route that expects DELETE request at the URI/endpoint  "/delete-users"
// The program should check if the user is existing before deleting 
// Response that will be received is the updated collection of users 
// Code below...
app.delete("/delete-users", (request, response) => {
    let message;

    for(let i = 0; i < users.length; i++){
        if(request.body.username == users[i].username){
            users.splice(i, 1);
            message = "Successfully deleted"
        }
        else {
        	message = "User does not exist."
        }
    }
    console.log(users);
    response.send(message);
});


// Save all the successful request tabs and export the collection
// Update your Gitlab repor and link it to Boodle 







// Tells our server/application to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the serves running in the terminal
app.listen(port, () => console.log(`Server running at port ${port}`));